package com.quanta.mojo;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.ws.rs.core.MediaType;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;

/**
 * Goal which touches a timestamp file.
 * 
 * @goal deploy
 * 
 * @phase package
 */
public class MyMojo extends AbstractMojo {
	/**
	 * Location of the file.
	 * 
	 * @parameter expression="${project.build.directory}"
	 * @required
	 */
	private File outputDirectory;

	/**
	 * @parameter expression="${project.build.finalName}"
	 * @required
	 */
	private String warName;

	/**
	 * @parameter expression="${project.packaging}"
	 * @required
	 */
	private String packaging;

	/**
	 * @parameter expression="${project.version}"
	 * @required
	 */
	private String version;

	/**
	 * @parameter
	 */
	private String nebulaLocation;

	/**
	 * @parameter
	 */
	private String productId;

	/**
	 * @parameter
	 */
	private String applicationId;

	private String account;
	private String authorityCode;

	/**
	 * @return
	 */
	private String getUploadUrl() {

		// return String.format("http://%1s/product/%2s/deploy/%3s",
		// nebulaLocation, productId, version);

		return String.format("http://%1s/deploy/%2s", nebulaLocation, version);
	}

	/**
	 * 上傳 package 到 Nebula 平台環境上。
	 * 
	 * <p/>
	 * 程式流程：
	 * <ul>
	 * <li>取得目前環境中，打包的檔案名稱</li>
	 * <li>確認檔案中有 META-INF/nebula.properties 檔案，而且從裏面抓出要佈署的 productId,
	 * applicationId 等資訊</li>
	 * <li>確認 META-INF 目錄中，沒有 context.xml 的檔案存在 (for tomcat deploy~)</li>
	 * <li>版本會使用 pom.xml 的版本？？？ 還是要另外設定上傳版本號？</li>
	 * <li>確認好要上傳的 url，並設定好參數</li>
	 * <li></li>
	 * </ul>
	 * 
	 * @see org.apache.maven.plugin.AbstractMojo#execute()
	 */
	public void execute() throws MojoExecutionException {

		// Prepare Data
		getLog().info(packaging);
		if (!"war".equals(packaging)) {
			throw new MojoExecutionException("Need war");
		}

		File f = new File(outputDirectory, warName + ".war");
		getLog().info(f.getAbsolutePath());

		loadNebulaProperties(f);

		getLog().info(getUploadUrl());

		// Create RESTful Client
		// TODO: 這部份呼叫 Service 的部份也重複了，是不是針對 RESTful Client 的部份要獨立成一個 jar? 方便使用？
		Client client = Client.create();
		client.addFilter(new ClientFilter() {
			@Override
			public ClientResponse handle(ClientRequest cr) throws ClientHandlerException {

				cr.getHeaders().putSingle("Nebula-ProductId", productId);
				cr.getHeaders().putSingle("Nebula-ApplicationId", applicationId);

				return getNext().handle(cr);
			}
		});

		// Upload File to Nebula Platform
		FormDataMultiPart form = new FormDataMultiPart();
		form.getBodyParts().add(new FileDataBodyPart("deployfile", f));

		WebResource webResource = client.resource(getUploadUrl());
		ClientResponse resp = webResource.type(MediaType.MULTIPART_FORM_DATA)
				.accept(MediaType.TEXT_PLAIN)
				.post(ClientResponse.class, form);

		getLog().info("Status Code: " + resp.getStatus());
		getLog().info("Resp: " + resp.getEntity(String.class));
	}

	/**
	 * 載入 SaaS 中的 nebula.properties 檔案，並且讀出內容資料。
	 * 
	 * @param f
	 * @throws MojoExecutionException
	 */
	private void loadNebulaProperties(File f) throws MojoExecutionException {

		try {
			ZipFile warFile = new ZipFile(f);
			
//			Enumeration<? extends ZipEntry> entries = warFile.entries();
//			while(entries.hasMoreElements()) {
//				
//				getLog().debug(entries.nextElement().getName());
//			}
			ZipEntry nebulaPropEntry = warFile.getEntry("META-INF/nebula.properties");
			
			if (nebulaPropEntry == null) {
				throw new MojoExecutionException("\n need META-INF/nebula.properties");
			}
			
			InputStream is = warFile.getInputStream(nebulaPropEntry);
			Properties prop = new Properties();
			prop.load(is);
			
			nebulaLocation = prop.getProperty("cloudplatform.paas.location");
			productId = prop.getProperty("cloudplatform.solution.productId");
			applicationId = prop.getProperty("cloudplatform.solution.applicationId");
			
			account = prop.getProperty("cloudplatform.developer.account");
			authorityCode = prop.getProperty("cloudplatform.developer.authorityCode");
		}
		catch (Exception e) {
			getLog().error(e);
			throw new MojoExecutionException("can't open war file: " + f, e);
		}
	}
}
