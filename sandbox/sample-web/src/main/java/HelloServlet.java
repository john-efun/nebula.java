import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.quanta.module.web.NebulaServletContextListener;

/**
 * Servlet implementation class HelloServlet
 */
@WebServlet("/HelloServlet")
public class HelloServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = Logger.getLogger(this.getClass());

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HelloServlet() {

		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Object appContext = request.getSession().getAttribute(
				NebulaServletContextListener.NEUBLA_APPCONTEXT);

		logger.debug(appContext);

		String name = "";
		if (request.getParameterMap().containsKey("name")) {
			name = request.getParameter("name");
		}

		response.getOutputStream().println(
				"<h1>Hello " + name + " </h1><br/>" + appContext);

	}

}
