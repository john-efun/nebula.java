/**
 * 
 */
package com.quanta.kernel.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quanta.kernel.repository.ICommonRepository;
import com.sun.jersey.api.NotFoundException;

/**
 * 以 Guid 來取得 SolutionId 的對應。
 * 
 * @author u99122010
 * 
 */
@Path("/solution")
@Component
public class SolutionResolveService {

	@Autowired
	private ICommonRepository repository;

	/**
	 * 根據 Guid 呼叫 {@link ICommonRepository#findSolutionIdByGuid(String)} 取得對應的
	 * SolutionId
	 * 
	 * @param guid
	 * @return
	 */
	@GET
	@Path("/{guid}")
	@Produces(MediaType.TEXT_PLAIN)
	public String Resolve(@PathParam("guid") String guid) {

		String solutionId = repository.findSolutionIdByGuid(guid);

		// 如果沒有找到資料，回應 404 not found
		if (solutionId == null) {
			throw new NotFoundException("");
		}

		// 如果查詢成功，就回傳 Solution Id 
		return solutionId;
	}
}
