/**
 * 
 */
package com.quanta.kernel.repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author u99122010
 * 
 */
//@Entity
//@Table(name = "ErrorLogParam")
public class ErrorLog implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private long logId;
	private String solutionId;
	private String productId;
	private String applicationId;
	private String isProduction;
	private String assemblyName;
	private String typeName;
	private String methodName;
	private String machineName;
	private String clientInformation;
	private String clientIp;
	private String serverIp;
	private String errorCode;
	private String errorTypeName;
	private String errorDescription;
	private String stackTrace;
	private String errorLevel;
	private String class1;
	private String class2;
	private String class3;
	private String errorStatus;
	private String isSendToMom;
	private Date createDate;
	private Date modifyDate;
	private Date clientLogTime;
	private String userId;

	@OneToMany
	private List<ErrorLogParam> params;

	/**
	 * @return the logId
	 */
	public long getLogId() {

		return logId;
	}

	/**
	 * @param logId
	 *            the logId to set
	 */
	public void setLogId(long logId) {

		this.logId = logId;
	}

	/**
	 * @return the solutionId
	 */
	public String getSolutionId() {

		return solutionId;
	}

	/**
	 * @param solutionId
	 *            the solutionId to set
	 */
	public void setSolutionId(String solutionId) {

		this.solutionId = solutionId;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {

		return productId;
	}

	/**
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(String productId) {

		this.productId = productId;
	}

	/**
	 * @return the applicationId
	 */
	public String getApplicationId() {

		return applicationId;
	}

	/**
	 * @param applicationId
	 *            the applicationId to set
	 */
	public void setApplicationId(String applicationId) {

		this.applicationId = applicationId;
	}

	/**
	 * @return the isProduction
	 */
	public String getIsProduction() {

		return isProduction;
	}

	/**
	 * @param isProduction
	 *            the isProduction to set
	 */
	public void setIsProduction(String isProduction) {

		this.isProduction = isProduction;
	}

	/**
	 * @return the assemblyName
	 */
	public String getAssemblyName() {

		return assemblyName;
	}

	/**
	 * @param assemblyName
	 *            the assemblyName to set
	 */
	public void setAssemblyName(String assemblyName) {

		this.assemblyName = assemblyName;
	}

	/**
	 * @return the typeName
	 */
	public String getTypeName() {

		return typeName;
	}

	/**
	 * @param typeName
	 *            the typeName to set
	 */
	public void setTypeName(String typeName) {

		this.typeName = typeName;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {

		return methodName;
	}

	/**
	 * @param methodName
	 *            the methodName to set
	 */
	public void setMethodName(String methodName) {

		this.methodName = methodName;
	}

	/**
	 * @return the machineName
	 */
	public String getMachineName() {

		return machineName;
	}

	/**
	 * @param machineName
	 *            the machineName to set
	 */
	public void setMachineName(String machineName) {

		this.machineName = machineName;
	}

	/**
	 * @return the clientInformation
	 */
	public String getClientInformation() {

		return clientInformation;
	}

	/**
	 * @param clientInformation
	 *            the clientInformation to set
	 */
	public void setClientInformation(String clientInformation) {

		this.clientInformation = clientInformation;
	}

	/**
	 * @return the clientIp
	 */
	public String getClientIp() {

		return clientIp;
	}

	/**
	 * @param clientIp
	 *            the clientIp to set
	 */
	public void setClientIp(String clientIp) {

		this.clientIp = clientIp;
	}

	/**
	 * @return the serverIp
	 */
	public String getServerIp() {

		return serverIp;
	}

	/**
	 * @param serverIp
	 *            the serverIp to set
	 */
	public void setServerIp(String serverIp) {

		this.serverIp = serverIp;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {

		return errorCode;
	}

	/**
	 * @param errorCode
	 *            the errorCode to set
	 */
	public void setErrorCode(String errorCode) {

		this.errorCode = errorCode;
	}

	/**
	 * @return the errorTypeName
	 */
	public String getErrorTypeName() {

		return errorTypeName;
	}

	/**
	 * @param errorTypeName
	 *            the errorTypeName to set
	 */
	public void setErrorTypeName(String errorTypeName) {

		this.errorTypeName = errorTypeName;
	}

	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {

		return errorDescription;
	}

	/**
	 * @param errorDescription
	 *            the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {

		this.errorDescription = errorDescription;
	}

	/**
	 * @return the stackTrace
	 */
	public String getStackTrace() {

		return stackTrace;
	}

	/**
	 * @param stackTrace
	 *            the stackTrace to set
	 */
	public void setStackTrace(String stackTrace) {

		this.stackTrace = stackTrace;
	}

	/**
	 * @return the errorLevel
	 */
	public String getErrorLevel() {

		return errorLevel;
	}

	/**
	 * @param errorLevel
	 *            the errorLevel to set
	 */
	public void setErrorLevel(String errorLevel) {

		this.errorLevel = errorLevel;
	}

	/**
	 * @return the class1
	 */
	public String getClass1() {

		return class1;
	}

	/**
	 * @param class1
	 *            the class1 to set
	 */
	public void setClass1(String class1) {

		this.class1 = class1;
	}

	/**
	 * @return the class2
	 */
	public String getClass2() {

		return class2;
	}

	/**
	 * @param class2
	 *            the class2 to set
	 */
	public void setClass2(String class2) {

		this.class2 = class2;
	}

	/**
	 * @return the class3
	 */
	public String getClass3() {

		return class3;
	}

	/**
	 * @param class3
	 *            the class3 to set
	 */
	public void setClass3(String class3) {

		this.class3 = class3;
	}

	/**
	 * @return the errorStatus
	 */
	public String getErrorStatus() {

		return errorStatus;
	}

	/**
	 * @param errorStatus
	 *            the errorStatus to set
	 */
	public void setErrorStatus(String errorStatus) {

		this.errorStatus = errorStatus;
	}

	/**
	 * @return the isSendToMom
	 */
	public String getIsSendToMom() {

		return isSendToMom;
	}

	/**
	 * @param isSendToMom
	 *            the isSendToMom to set
	 */
	public void setIsSendToMom(String isSendToMom) {

		this.isSendToMom = isSendToMom;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {

		return createDate;
	}

	/**
	 * @param createDate
	 *            the createDate to set
	 */
	public void setCreateDate(Date createDate) {

		this.createDate = createDate;
	}

	/**
	 * @return the modifyDate
	 */
	public Date getModifyDate() {

		return modifyDate;
	}

	/**
	 * @param modifyDate
	 *            the modifyDate to set
	 */
	public void setModifyDate(Date modifyDate) {

		this.modifyDate = modifyDate;
	}

	/**
	 * @return the clientLogTime
	 */
	public Date getClientLogTime() {

		return clientLogTime;
	}

	/**
	 * @param clientLogTime
	 *            the clientLogTime to set
	 */
	public void setClientLogTime(Date clientLogTime) {

		this.clientLogTime = clientLogTime;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {

		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {

		this.userId = userId;
	}

	/**
	 * @return the params
	 */
	public List<ErrorLogParam> getParams() {

		return params;
	}

	/**
	 * @param params
	 *            the params to set
	 */
	public void setParams(List<ErrorLogParam> params) {

		this.params = params;
	}

}
