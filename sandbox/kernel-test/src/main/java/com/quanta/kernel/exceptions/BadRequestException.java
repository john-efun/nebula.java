/**
 * 
 */
package com.quanta.kernel.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * 回傳 Http Status 401 Bad Request
 * 
 * @author u99122010
 * 
 */
public class BadRequestException extends WebApplicationException {

	private static final long serialVersionUID = 1L;

	/**
	 * 建構子
	 */
	public BadRequestException() {

		this("");
	}

	public BadRequestException(String message) {

		super(Response.status(Status.BAD_REQUEST).entity(message).build());
	}
}
