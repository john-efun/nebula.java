/**
 * 
 */
package com.quanta.kernel.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * {@link ILogRepository} 的實作，主要針對 Log，包含 ErrorLog 以及 TransactionLog 的紀錄.
 * 
 * @author u99122010
 * 
 */
public class LogRepositoryImpl implements ILogRepository {

	@PersistenceContext
	private EntityManager em;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.quanta.kernel.repository.ILogRepository#saveErrorLog(com.quanta.kernel
	 * .repository.ErrorLog)
	 */
	public void saveErrorLog(ErrorLog logger) {

		em.persist(logger);
		em.flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.quanta.kernel.repository.ILogRepository#saveTransactionLog(com.quanta
	 * .kernel.repository.TransactionLog)
	 */
	public void saveTransactionLog(TransactionLog transLog) {

		em.persist(transLog);
		em.flush();
	}
}
