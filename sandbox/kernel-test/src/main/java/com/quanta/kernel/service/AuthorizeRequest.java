/**
 * 
 */
package com.quanta.kernel.service;

import java.io.Serializable;

/**
 * Authorize 的 Request 資料。
 * 
 * @author u99122010
 *
 */
public class AuthorizeRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String account;
	private String authorityCode;
	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}
	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}
	/**
	 * @return the authorityCode
	 */
	public String getAuthorityCode() {
		return authorityCode;
	}
	/**
	 * @param authorityCode the authorityCode to set
	 */
	public void setAuthorityCode(String authorityCode) {
		this.authorityCode = authorityCode;
	}

}
