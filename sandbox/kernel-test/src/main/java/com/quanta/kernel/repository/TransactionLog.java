/**
 * 
 */
package com.quanta.kernel.repository;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author u99122010
 * 
 */
//@Entity
//@Table(name = "TransactionLog")
public class TransactionLog implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private long logId;
	private String solutionId;
	private String productId;
	private String applicationId;
	private String isProduction;
	private int threadId;
	private String functionId;
	private String columnName;
	private String oldData;
	private String newDate;
	private String sqlComand;
	private String remarkText1;
	private String remarkText2;
	private String remarkText3;
	private String remarkText4;
	private BigDecimal remarkInt1;
	private BigDecimal remarkInt2;
	private BigDecimal remarkInt3;
	private BigDecimal remarkInt4;
	private Date createDate;
	private String createUser;

	/**
	 * @return the logId
	 */
	public long getLogId() {

		return logId;
	}

	/**
	 * @param logId
	 *            the logId to set
	 */
	public void setLogId(long logId) {

		this.logId = logId;
	}

	/**
	 * @return the solutionId
	 */
	public String getSolutionId() {

		return solutionId;
	}

	/**
	 * @param solutionId
	 *            the solutionId to set
	 */
	public void setSolutionId(String solutionId) {

		this.solutionId = solutionId;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {

		return productId;
	}

	/**
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(String productId) {

		this.productId = productId;
	}

	/**
	 * @return the applicationId
	 */
	public String getApplicationId() {

		return applicationId;
	}

	/**
	 * @param applicationId
	 *            the applicationId to set
	 */
	public void setApplicationId(String applicationId) {

		this.applicationId = applicationId;
	}

	/**
	 * @return the isProduction
	 */
	public String getIsProduction() {

		return isProduction;
	}

	/**
	 * @param isProduction
	 *            the isProduction to set
	 */
	public void setIsProduction(String isProduction) {

		this.isProduction = isProduction;
	}

	/**
	 * @return the threadId
	 */
	public int getThreadId() {

		return threadId;
	}

	/**
	 * @param threadId
	 *            the threadId to set
	 */
	public void setThreadId(int threadId) {

		this.threadId = threadId;
	}

	/**
	 * @return the functionId
	 */
	public String getFunctionId() {

		return functionId;
	}

	/**
	 * @param functionId
	 *            the functionId to set
	 */
	public void setFunctionId(String functionId) {

		this.functionId = functionId;
	}

	/**
	 * @return the columnName
	 */
	public String getColumnName() {

		return columnName;
	}

	/**
	 * @param columnName
	 *            the columnName to set
	 */
	public void setColumnName(String columnName) {

		this.columnName = columnName;
	}

	/**
	 * @return the oldData
	 */
	public String getOldData() {

		return oldData;
	}

	/**
	 * @param oldData
	 *            the oldData to set
	 */
	public void setOldData(String oldData) {

		this.oldData = oldData;
	}

	/**
	 * @return the newDate
	 */
	public String getNewDate() {

		return newDate;
	}

	/**
	 * @param newDate
	 *            the newDate to set
	 */
	public void setNewDate(String newDate) {

		this.newDate = newDate;
	}

	/**
	 * @return the sqlComand
	 */
	public String getSqlComand() {

		return sqlComand;
	}

	/**
	 * @param sqlComand
	 *            the sqlComand to set
	 */
	public void setSqlComand(String sqlComand) {

		this.sqlComand = sqlComand;
	}

	/**
	 * @return the remarkText1
	 */
	public String getRemarkText1() {

		return remarkText1;
	}

	/**
	 * @param remarkText1
	 *            the remarkText1 to set
	 */
	public void setRemarkText1(String remarkText1) {

		this.remarkText1 = remarkText1;
	}

	/**
	 * @return the remarkText2
	 */
	public String getRemarkText2() {

		return remarkText2;
	}

	/**
	 * @param remarkText2
	 *            the remarkText2 to set
	 */
	public void setRemarkText2(String remarkText2) {

		this.remarkText2 = remarkText2;
	}

	/**
	 * @return the remarkText3
	 */
	public String getRemarkText3() {

		return remarkText3;
	}

	/**
	 * @param remarkText3
	 *            the remarkText3 to set
	 */
	public void setRemarkText3(String remarkText3) {

		this.remarkText3 = remarkText3;
	}

	/**
	 * @return the remarkText4
	 */
	public String getRemarkText4() {

		return remarkText4;
	}

	/**
	 * @param remarkText4
	 *            the remarkText4 to set
	 */
	public void setRemarkText4(String remarkText4) {

		this.remarkText4 = remarkText4;
	}

	/**
	 * @return the remarkInt1
	 */
	public BigDecimal getRemarkInt1() {

		return remarkInt1;
	}

	/**
	 * @param remarkInt1
	 *            the remarkInt1 to set
	 */
	public void setRemarkInt1(BigDecimal remarkInt1) {

		this.remarkInt1 = remarkInt1;
	}

	/**
	 * @return the remarkInt2
	 */
	public BigDecimal getRemarkInt2() {

		return remarkInt2;
	}

	/**
	 * @param remarkInt2
	 *            the remarkInt2 to set
	 */
	public void setRemarkInt2(BigDecimal remarkInt2) {

		this.remarkInt2 = remarkInt2;
	}

	/**
	 * @return the remarkInt3
	 */
	public BigDecimal getRemarkInt3() {

		return remarkInt3;
	}

	/**
	 * @param remarkInt3
	 *            the remarkInt3 to set
	 */
	public void setRemarkInt3(BigDecimal remarkInt3) {

		this.remarkInt3 = remarkInt3;
	}

	/**
	 * @return the remarkInt4
	 */
	public BigDecimal getRemarkInt4() {

		return remarkInt4;
	}

	/**
	 * @param remarkInt4
	 *            the remarkInt4 to set
	 */
	public void setRemarkInt4(BigDecimal remarkInt4) {

		this.remarkInt4 = remarkInt4;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {

		return createDate;
	}

	/**
	 * @param createDate
	 *            the createDate to set
	 */
	public void setCreateDate(Date createDate) {

		this.createDate = createDate;
	}

	/**
	 * @return the createUser
	 */
	public String getCreateUser() {

		return createUser;
	}

	/**
	 * @param createUser
	 *            the createUser to set
	 */
	public void setCreateUser(String createUser) {

		this.createUser = createUser;
	}

}
