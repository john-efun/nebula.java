package com.quanta.kernel.service;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.inject.Alternative;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Component;

import com.quanta.kernel.exceptions.BadRequestException;

/**
 * @author ricky.chiang@quantatw.com
 * @version 2011/10/14
 * 
 */
@Component
@Path("/deploy")
public class ProductDeployService {

	// Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * @param productId
	 * @return
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String GetDeployInfo(@PathParam("productId") String productId) {

		return productId + ": version";
	}

	/**
	 * 佈署檔案。
	 * 
	 * <p/>
	 * 功能規格：
	 * <ul>
	 * <li>確認是否有上傳檔案</li>
	 * <li>根據 ProductId, 確認資料庫的資料是否正確</li>
	 * <li>檢查上傳檔案的規格，是否符合</li>
	 * <li>確認版本，是否可以更新 (如果相同的版本號已經更新，確認是否可以更新。如果已經審核，就不可更新)</li>
	 * <li>檢查佈署檔案的內容，是否根據 SaaS 的規範</li>
	 * <li>將檔案存檔到 PaaSDeploy 等待審核、佈署</li>
	 * </ul>
	 * <p/>
	 * 
	 * @param productId
	 *            產品代碼
	 * @param version
	 *            版本號
	 * @param request
	 *            {@link HttpServletRequest} 物件，用來取得上傳的檔案
	 * @return <ul>
	 *         <li>200: 成功</li> <li>304: 該版本已經通過審核，不可再更新</li> <li>400: 更新失敗，請參考
	 *         response body 的訊息</li> <li>404: 無法找到 Product 的資料</li>
	 *         </ul>
	 */
	@SuppressWarnings("unchecked")
	@POST
	@Path("/{version}")
	@Produces(MediaType.TEXT_PLAIN)
	public String DeployFile(@Context HttpServletRequest request, @HeaderParam("Nebula-ProductId") String productId,
			@HeaderParam("Nebula-ApplicationId") String applicationId, @PathParam("version") String version) {

		System.out.println("productId: " + productId);
		System.out.println("applicationId: " + applicationId);
		System.out.println("version: " + version);

		// TODO: 目前是直接放到一個路徑，後續要放到 PaaSDeploy 檔案中
		// String fileRepository = System.getProperty("java.io.tmpdir");
		String fileRepository = "/usr/local/lib/apache-tomcat-7.0.21/webapps";

		if (ServletFileUpload.isMultipartContent(request)) {
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);

			List<FileItem> items = null;
			try {
				items = upload.parseRequest(request);
			}
			catch (FileUploadException e) {
				e.printStackTrace();
				throw new BadRequestException("parse request failed");
			}

			if (items != null) {
				Iterator<FileItem> iter = items.iterator();

				while (iter.hasNext()) {
					FileItem item = iter.next();

					if (!item.isFormField() && item.getSize() > 0) {
						if (!"deployfile".equals(item.getFieldName())) {
							throw new BadRequestException("please upload deploy file!!!");
						}
						System.out.println(item.getFieldName());

						String fileName = processFileName(item.getName());

						System.out.println(fileRepository + "/" + fileName);

						try {
							item.write(new File(fileRepository, productId + ".war"));
						}
						catch (Exception e) {
							e.printStackTrace();
							throw new BadRequestException("save deploy failed!!!");
						}

						// if Success, Return blank~ Http200
						return "";
					}
				}

			}

			throw new BadRequestException("no file content!!!");
		}
		else {
			throw new BadRequestException("need multipart/form-data content type!!!");
		}

	}

	private String processFileName(String fileNameInput) {

		String fileNameOutput = null;
		fileNameOutput = fileNameInput.substring(fileNameInput.lastIndexOf("\\") + 1, fileNameInput.length());
		return fileNameOutput;
	}
}
