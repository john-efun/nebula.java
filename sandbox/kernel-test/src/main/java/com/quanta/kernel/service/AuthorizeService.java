package com.quanta.kernel.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quanta.kernel.exceptions.BadRequestException;
import com.quanta.kernel.exceptions.UserNotAuthorizeException;
import com.quanta.kernel.repository.ICommonRepository;

/**
 * Authorize 驗證，用來驗證使用者是否有權限可以開發 Product。
 * 
 * @author u99122010
 * 
 */
@Path("/access")
@Component
public class AuthorizeService {

	@Autowired
	private ICommonRepository repository;

	/**
	 * 驗證使用者與 ProductId 的對應.
	 * 
	 * 這個方法基本上就是一連串的檢查，如果檢核都通過，表示驗證成功.
	 * 
	 * @param request 傳入 account 與 authorityCode 
	 * @param productId 由 Request Header 傳入，Header Name 為 "Nebula-ProductId"
	 * @return 如果驗證都成功，回傳一個空字串就可以，讓 Status 變成 ok(200)
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public String Authorize(AuthorizeRequest request, @HeaderParam("Nebula-ProductId") String productId) {

		// System.out.println(productId);

		// String productId =
		// headers.getRequestHeaders().getFirst("Nebula-ProductId");
		// Check Request Header
		if (StringUtils.isEmpty(productId)) {
			throw new BadRequestException("Need Request Header \"Nebula-ProductId\"");
		}
		// Check Request Data
		if (StringUtils.isEmpty(request.getAccount())) {
			throw new BadRequestException("Need account");
		}
		if (StringUtils.isEmpty(request.getAuthorityCode())) {
			throw new BadRequestException("Need authorityCode");
		}

		// process
		if (!repository.CheckUserAuthority(productId, request.getAccount(), request.getAuthorityCode())) {
			throw new UserNotAuthorizeException();
		}
		
		return "";
	}

}
