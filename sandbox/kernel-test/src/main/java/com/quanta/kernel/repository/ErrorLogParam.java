/**
 * 
 */
package com.quanta.kernel.repository;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author u99122010
 *
 */
//@Entity
//@Table(name = "ErrorLogParam")
public class ErrorLogParam implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	private long logParamId;
	
	@ManyToOne
	@Column(columnDefinition="logId")
	private ErrorLog errorLog;
	
	private String type;
	private boolean inOut;
	private String value;
	private String name;
	

	/**
	 * @return the logParamId
	 */
	public long getLogParamId() {
	
		return logParamId;
	}
	/**
	 * @param logParamId the logParamId to set
	 */
	public void setLogParamId(long logParamId) {
	
		this.logParamId = logParamId;
	}
	/**
	 * @return the errorLog
	 */
	public ErrorLog getErrorLog() {
	
		return errorLog;
	}
	/**
	 * @param errorLog the errorLog to set
	 */
	public void setErrorLog(ErrorLog errorLog) {
	
		this.errorLog = errorLog;
	}
	/**
	 * @return the type
	 */
	public String getType() {
	
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
	
		this.type = type;
	}
	/**
	 * @return the inOut
	 */
	public boolean isInOut() {
	
		return inOut;
	}
	/**
	 * @param inOut the inOut to set
	 */
	public void setInOut(boolean inOut) {
	
		this.inOut = inOut;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
	
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
	
		this.value = value;
	}
	/**
	 * @return the name
	 */
	public String getName() {
	
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
	
		this.name = name;
	}
}
