package com.quanta.kernel.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * 授權驗證失敗的錯誤，對應 HttpStatus 401 UnAuthorized
 * 
 * @author 99122010
 * 
 */
public class UserNotAuthorizeException extends WebApplicationException {

	private static final long serialVersionUID = 1L;

	public UserNotAuthorizeException() {
		super(Response.status(Status.UNAUTHORIZED).entity("").build());
	}

}