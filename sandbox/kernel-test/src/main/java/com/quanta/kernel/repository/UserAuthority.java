/**
 * 
 */
package com.quanta.kernel.repository;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * 使用者授權資料物件。
 * 
 * @author 99122010
 * 
 */
@Entity
@Table(name = "UseAuthority")
@NamedQueries({ @NamedQuery(name = "findForAuthorize", query = "from UserAuthority where tempProductId=:productId and userId=:account and authorityCode=:authorityCode") })
public class UserAuthority implements Serializable {

	private static final long serialVersionUID = 1L;

	private long authorityId;
	private String tempProductId;
	private String userId;
	private String authorityType;
	private String authorityCode;
	private String applicationId;
	private Date startDate;
	private Date endDate;

	/**
	 * @return the authorityId
	 */
	@Id
	public long getAuthorityId() {
		return authorityId;
	}

	/**
	 * @param authorityId
	 *            the authorityId to set
	 */
	public void setAuthorityId(long authorityId) {
		this.authorityId = authorityId;
	}

	/**
	 * @return the tempProductId
	 */
	public String getTempProductId() {
		return tempProductId;
	}

	/**
	 * @param tempProductId
	 *            the tempProductId to set
	 */
	public void setTempProductId(String tempProductId) {
		this.tempProductId = tempProductId;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the authorityType
	 */
	public String getAuthorityType() {
		return authorityType;
	}

	/**
	 * @param authorityType
	 *            the authorityType to set
	 */
	public void setAuthorityType(String authorityType) {
		this.authorityType = authorityType;
	}

	/**
	 * @return the authorityCode
	 */
	public String getAuthorityCode() {
		return authorityCode;
	}

	/**
	 * @param authorityCode
	 *            the authorityCode to set
	 */
	public void setAuthorityCode(String authorityCode) {
		this.authorityCode = authorityCode;
	}

	/**
	 * @return the applicationId
	 */
	public String getApplicationId() {
		return applicationId;
	}

	/**
	 * @param applicationId
	 *            the applicationId to set
	 */
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
