package com.quanta.kernel.repository;

/**
 * Log 資料存取介面.
 * 
 * @author u99122010
 *
 */
public interface ILogRepository {

	/**
	 * 將 ErrorLog 存入資料庫中
	 * 
	 * @param logger
	 */
	public abstract void saveErrorLog(ErrorLog logger);

	/**
	 * 將 TransactionLog 存入資料庫
	 * 
	 * @param transLog
	 */
	public abstract void saveTransactionLog(TransactionLog transLog);

}