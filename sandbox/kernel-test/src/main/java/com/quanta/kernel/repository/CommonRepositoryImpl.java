/**
 * 
 */
package com.quanta.kernel.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

/**
 * 共用資料的存取 Repository 實作。
 * 
 * @author u99122010
 * 
 */
@Repository
public class CommonRepositoryImpl implements ICommonRepository {

	@PersistenceContext
	private EntityManager em;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.quanta.kernel.repository.IUserAuthorizeRepository#CheckUserAuthority
	 * (java.lang.String, java.lang.String, java.lang.String)
	 */
	public boolean CheckUserAuthority(String productId, String userAccount, String authorityCode) {

		// Query UseAuthority and found count, check data exist

		int count = em.createNamedQuery("findForAuthorize")
				.setParameter("productId", productId)
				.setParameter("account", userAccount)
				.setParameter("authorityCode", authorityCode)
				.getResultList()
				.size();

		return (count > 0);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.quanta.kernel.repository.ICommonRepository#findSolutionIdByGuid(java
	 * .lang.String)
	 */
	public String findSolutionIdByGuid(String guid) {

		GuidSolutionMapping mapping = em.find(GuidSolutionMapping.class, guid);

		return (mapping != null) ? mapping.getSolutionId() : null;
	}

}
