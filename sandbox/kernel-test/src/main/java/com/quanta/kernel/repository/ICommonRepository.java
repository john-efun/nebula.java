/**
 * 
 */
package com.quanta.kernel.repository;

/**
 * 共用資料的存取 Repository 介面, 如 {@link UserAuthority}, {@link GuidSolutionMapping}
 * 等資料。
 * 
 * @author u99122010
 * 
 */
public interface ICommonRepository {

	/**
	 * 驗證使用者是否為傳入 ProductId 的開發者.
	 * 
	 * @param productId
	 * @param userAccount
	 * @param authorityCode
	 * @return true 表示該使用者有此 Product 開發者的權限，false 表示沒有
	 */
	public abstract boolean CheckUserAuthority(String productId,
			String userAccount, String authorityCode);

	/**
	 * 以 Guid 取得對應的 SolutionId 資料。
	 * 
	 * @param guid
	 * @return 回傳對應的 SolutionId, 如果找不到，回傳 null
	 */
	public String findSolutionIdByGuid(String guid);
}
