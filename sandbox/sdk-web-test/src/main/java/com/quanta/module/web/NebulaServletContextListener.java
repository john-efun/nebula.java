// ~ Package Declaration
// ==================================================
/**
 * 
 */
package com.quanta.module.web;

import java.lang.ref.WeakReference;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quanta.module.AppContext;
import com.quanta.module.IAppContext;

// ~ Comments
// ==================================================
/**
 * @author ricky.chiang@quantatw.com
 * 
 */
public class NebulaServletContextListener implements ServletContextListener, HttpSessionListener {

	// ~ Static Members
	// ==================================================
	public final static String NEBULA_DEVELOPER_AUTHENTICACTED = "NEBULA_DEVELOPER_AUTHENTICACTED";
	public final static String NEUBLA_APPCONTEXT = "NEUBLA_APP_CONTEXT";

	private final static Logger Logger = LoggerFactory.getLogger(NebulaServletContextListener.class);

	// ~ Fields
	// ==================================================

	// ~ Constructors
	// ==================================================

	// ~ Methods
	// ==================================================
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.ServletContextListener#contextInitialized(javax.servlet
	 * .ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent sce) {

		ServletContext servletContext = sce.getServletContext();

		NebulaWebFilter.servletContext = new WeakReference<ServletContext>(servletContext);

		// 使用 AppContext 的機制，來驗證使用者
		IAppContext appContext = new AppContext();
		if (!AppContext.isAuthorized()) {
			String msg = "Developer authenticate failed!!! Pls check nebula.properties";

			Logger.debug(msg);
			throw new RuntimeException(msg);
		}

		servletContext.setAttribute(NEBULA_DEVELOPER_AUTHENTICACTED, AppContext.isAuthorized());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.
	 * ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent sce) {

		sce.getServletContext().removeAttribute(NEBULA_DEVELOPER_AUTHENTICACTED);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http
	 * .HttpSessionEvent)
	 */
	public void sessionCreated(HttpSessionEvent se) {

		HttpSession session = se.getSession();

		IAppContext appContext = new AppContext();

		// Save AppContext By Session
		// session.setAttribute(NEBULA_DEVELOPER_AUTHENTICACTED,
		// appContext.isAuthorized());
		session.setAttribute(NEUBLA_APPCONTEXT, appContext);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet
	 * .http.HttpSessionEvent)
	 */
	public void sessionDestroyed(HttpSessionEvent se) {

		se.getSession().removeAttribute(NEUBLA_APPCONTEXT);
	}
}
