// ~ Package Declaration
// ==================================================
/**
 * 
 */
package com.quanta.module.web;

import java.io.IOException;
import java.lang.ref.WeakReference;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import com.quanta.module.IAppContext;

// ~ Comments
// ==================================================
/**
 * @author ricky.chiang@quantatw.com
 * 
 */
public class NebulaWebFilter implements Filter {

	// ~ Static Members
	// ==================================================
	public final static String NEBULA_CONTEXT = IAppContext.class.getName();

	static volatile WeakReference<ServletContext> servletContext = new WeakReference<ServletContext>(null);

	/**
	 * @return
	 */
	public static boolean isNebulaDeveloperAuthenticated() {

		if (servletContext.get() != null) {
			Object isAuthenticated = servletContext.get().getAttribute(
					NebulaServletContextListener.NEBULA_DEVELOPER_AUTHENTICACTED);
			
			// Check if no AppContext authenticate....
			if (isAuthenticated == null) return false;
			
			return (Boolean) isAuthenticated;
		}
		return false;
	}

	// ~ Fields
	// ==================================================

	// ~ Constructors
	// ==================================================

	// ~ Methods
	// ==================================================
	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {

		// Close Filter
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 * javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException,
			ServletException {

		// 如果之前沒有驗證通過，要拋出錯誤訊息
		if (!isNebulaDeveloperAuthenticated()) {
			HttpServletResponse response = (HttpServletResponse)resp;
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Nebula authenticated failed!!!");
			return;
		}
		
		// Pre set appContext to every request
		// req.setAttribute(NEBULA_CONTEXT, new AppContext());

		chain.doFilter(req, resp);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void init(FilterConfig config) throws ServletException {

	}
}
