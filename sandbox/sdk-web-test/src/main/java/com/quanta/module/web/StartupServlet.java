// ~ Package Declaration
// ==================================================
/**
 * 
 */
package com.quanta.module.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

// ~ Comments
// ==================================================
/**
 * @author ricky.chiang@quantatw.com
 * 
 */

public class StartupServlet extends HttpServlet {

	// ~ Static Members
	// ==================================================

	private static final long serialVersionUID = 1L;

	private final static String QUERY_GUID = "v";
	private final static String DEFAULT_PAGE = "p";

	public final static String NEBULA_GUID = "__nebula_guid__";

	// ~ Fields
	// ==================================================

	// ~ Constructors
	// ==================================================

	// ~ Methods
	// ==================================================
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		if (req.getParameter(QUERY_GUID) != null && req.getParameter(DEFAULT_PAGE) != null) {

			HttpSession session = req.getSession(true);
			session.setAttribute(NEBULA_GUID, req.getParameter(QUERY_GUID));
			
			// redirect default page
			resp.sendRedirect(req.getParameter(DEFAULT_PAGE));
		}
		else {

			throw new RuntimeException("");
		}

		super.doGet(req, resp);
	}
}
