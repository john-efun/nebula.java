// ~ Package Declaration
// ==================================================
package com.quanta.module;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

// ~ Comments
// ==================================================
public class AppContextTest {

	// ~ Static Members
	// ==================================================
	static IAppContext appContext;

	// ~ Fields
	// ==================================================
	
	// ~ Constructors
	// ==================================================

	// ~ Methods
	// ==================================================
	@BeforeClass
	public static void onSetUp() {

		appContext = new AppContext();
	}

	@Test
	public void testGetModule() {

		ILogModule loggerModule = appContext.getModule(ILogModule.class);

		Assert.assertNotNull(loggerModule);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetNoImplementationModule() {

		appContext.getModule(ISampleModule.class);
	}

	@Test
	public void testDevelopAuthenticate() {

		Assert.assertTrue(AppContext.isAuthorized());
	}
}

interface ISampleModule {
}