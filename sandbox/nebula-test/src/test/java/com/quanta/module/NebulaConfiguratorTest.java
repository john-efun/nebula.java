package com.quanta.module;

import org.junit.Test;

import junit.framework.Assert;

/**
 * 測試讀取設定檔.
 * 
 * @author 99122010
 *
 */
public class NebulaConfiguratorTest {

	@Test
	public void testLoadConfig() {
		NebulaConfigurator config = NebulaConfigurator.INSTANCE;
		
		Assert.assertEquals("localhost:8088", config.getKernelServiceLocation());
		Assert.assertEquals("Northwind", config.getProductId());
		Assert.assertEquals("Northwind", config.getApplicationId());
		Assert.assertEquals("kent_chen@quantatw.com", config.getDevelopAccount());
		Assert.assertEquals("ABCDEFG", config.getAuthorityCode());
	}
}
