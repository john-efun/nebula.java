/**
 * 
 */
package com.quanta.module.kernel;

/**
 * @author 99122010
 * 
 */
public class AuthorizeReq {
	
	private String account;
	private String authorityCode;
	
	public AuthorizeReq(String account, String authorityCode) {

		this.account = account;
		this.authorityCode = authorityCode;
	}

	/**
	 * @return the account
	 */
	public String getAccount() {

		return account;
	}

	/**
	 * @param account
	 *            the account to set
	 */
	public void setAccount(String account) {

		this.account = account;
	}

	/**
	 * @return the authorityCode
	 */
	public String getAuthorityCode() {

		return authorityCode;
	}

	/**
	 * @param authorityCode
	 *            the authorityCode to set
	 */
	public void setAuthorityCode(String authorityCode) {

		this.authorityCode = authorityCode;
	}

}
