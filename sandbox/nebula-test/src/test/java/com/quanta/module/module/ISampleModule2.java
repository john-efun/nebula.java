package com.quanta.module.module;

import com.google.inject.ImplementedBy;

@ImplementedBy(SampleModuleImpl.class)
public interface ISampleModule2 {

	public abstract void testMethod2();
}
