package com.quanta.module.kernel;

import javax.ws.rs.core.MediaType;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.api.json.JSONConfiguration;

/**
 * 測試 Restful client 的使用方式，以及確認之前寫的測試 Service 都可以透過 Client 呼叫.
 * 
 * @author 99122010
 * 
 */
public class RestfulClientProxyTest {

	private Client client;
	private long testStart;

	@Before
	public void setup() {

		long startTime = System.currentTimeMillis();
		ClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);

		client = Client.create(config);
		System.out.printf("Create RESTful Client : %d ms\n", (System.currentTimeMillis() - startTime));

		client.addFilter(new LoggingFilter());

		testStart = System.currentTimeMillis();
	}

	@After
	public void cleanUp() {

		System.out.printf("Test Cose : %d ms\n", (System.currentTimeMillis() - testStart));
	}

	/**
	 * Test how to connect kernel restful service
	 */
	@Test
	public void testConnectToSolutionResolve() {

		WebResource webRes = client.resource("http://localhost:8088/solution/Northwind");

		// String solutionId = webRes.get(String.class);
		// System.out.println(solutionId);
		ClientResponse resp = webRes.get(ClientResponse.class);
		Assert.assertEquals(Status.OK.getStatusCode(), resp.getStatus());

		Assert.assertEquals("Northwind", resp.getEntity(String.class));
	}

	/**
	 * 測試錯誤的 Solution Id 取得
	 */
	@Test
	public void testWrongsolutionIdResolve() {

		WebResource webRes = client.resource("http://localhost:8088/solution/ABC");

		ClientResponse resp = webRes.get(ClientResponse.class);
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), resp.getStatus());
	}

	/**
	 * 測試傳入正確資料後，正確的驗證結果
	 */
	@Test
	public void testConnectToAuthorize() {

		String input = "{ \"account\" : \"kent_chen@quantatw.com\", \"authorityCode\": \"ABCDEFG\" }";

		WebResource res = client.resource("http://localhost:8088/access");

		client.addFilter(new ClientFilter() {
			@Override
			public ClientResponse handle(ClientRequest cr) throws ClientHandlerException {

				cr.getHeaders().putSingle("Nebula-ProductId", "Northwind");

				return getNext().handle(cr);
			}
		});

		ClientResponse resp = res.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, input);
		// System.out.println(resp.getEntity(String.class));

		Assert.assertEquals(Status.OK.getStatusCode(), resp.getStatus());
	}

	/**
	 * 測試沒有傳遞 ProductId 是否會正確拋出錯誤
	 */
	@Test
	public void testNoProductIdAuthorize() {

		String input = "{ \"account\" : \"kent_chen@quantatw.com\", \"authorityCode\": \"ABCDEFG\" }";
		WebResource res = client.resource("http://localhost:8088/access");

		ClientResponse resp = res.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, input);

		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), resp.getStatus());
	}

	/**
	 * 測試 Entity 轉換成 Json 是否可行～
	 */
	@Test
	public void testJsonWrapperClientRequest() {

		client.addFilter(new ClientFilter() {
			@Override
			public ClientResponse handle(ClientRequest cr) throws ClientHandlerException {

				cr.getHeaders().putSingle("Nebula-ProductId", "Northwind");

				return getNext().handle(cr);
			}
		});

		WebResource res = client.resource("http://localhost:8088/access");

		AuthorizeReq requestEntity = new AuthorizeReq("kent_chen@quantatw.com", "ABCDEFG");
		ClientResponse resp = res.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, requestEntity);

		Assert.assertEquals(Status.OK.getStatusCode(), resp.getStatus());
	}
}
