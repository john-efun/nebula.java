package com.quanta.module.module;

import com.google.inject.ImplementedBy;

@ImplementedBy(SampleModuleImpl.class)
public interface ISampleModule1 {

	public abstract void testMethod1();
}
