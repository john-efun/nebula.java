package com.quanta.module;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.Scope;
import com.google.inject.Scopes;

/**
 * 測試 {@link AppContextFactory}.
 * 
 * @author 99122010
 * 
 */
public class AppContextPoolTest {

	@Before
	public void setUp() {

		// 初始化 AppContextFactory~!?
		// AppContextFactory.getInstance();
	}

	/**
	 * 測試由 Factory 建立 {@link AppContext}
	 */
	@Test
	public void testCreateAppContextFromFactory() {

		IAppContext appContext = AppContext.getInstance();

		Assert.assertNotNull(appContext);
	}

	/**
	 * 測試不同 {@link Thread} 建立出來的 {@link AppContext} 應該不一樣.
	 */
	@Test
	public void testAppContextByThread() {

		TestAppContextThread.CleanPool();
		Thread t1 = new TestAppContextThread("app1");
		Thread t2 = new TestAppContextThread("app2");

		t1.start();
		t2.start();

		while (TestAppContextThread.Pools.size() < 2) {
			try {
				Thread.sleep(10);
			}
			catch (InterruptedException e) {
				// Wait Test Thread close
			}
		}

		Assert.assertNotSame(TestAppContextThread.Pools.get("app1"), TestAppContextThread.Pools.get("app2"));
	}

	/**
	 * 測試當變更 AppContextScope 的時候，可以按照設定的 {@link Scope} 來產生 {@link AppContext}.
	 */
	@Test
	public void testChangeAppContextScopeToSingleton() {

		// 變更 AppContextScope, 改成 Singleton, 應該是要每個 Thread 都一樣.
		AppContextFactory.getInstance().setAppContextScope(Scopes.SINGLETON);

		TestAppContextThread.CleanPool();
		Thread t1 = new TestAppContextThread("app1");
		Thread t2 = new TestAppContextThread("app2");

		t1.start();
		t2.start();

		while (TestAppContextThread.Pools.size() < 2) {
			try {
				Thread.sleep(10);
			}
			catch (InterruptedException e) {
				// Wait Test Thread close
			}
		}

		Assert.assertSame(TestAppContextThread.Pools.get("app1"), TestAppContextThread.Pools.get("app2"));
	}

	/**
	 * 測試用的 {@link Thread}, 主要要用來測試不同執行緒，產生 {@link AppContext} 的效果.
	 * 
	 * @author ricky.chiang@quantatw.com
	 * 
	 */
	private static class TestAppContextThread extends Thread {

		public static Map<String, IAppContext> Pools = new HashMap<String, IAppContext>();

		static void CleanPool() {

			Pools.clear();
		}

		/**
		 * @param name
		 */
		public TestAppContextThread(String name) {

			super(name);
		}

		@Override
		public void run() {

			System.out.println("Start " + getName());
			IAppContext context = AppContext.getInstance();
			System.out.println(context);
			Pools.put(getName(), context);
		}
	}

}
