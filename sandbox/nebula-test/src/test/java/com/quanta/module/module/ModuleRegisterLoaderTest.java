package com.quanta.module.module;

import junit.framework.Assert;

import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * @author 99122010
 * 
 */
public class ModuleRegisterLoaderTest {

	@Test
	public void testGetModules() {

		Injector injector = Guice.createInjector(new TestModule());
		
		Assert.assertNotNull(injector);
		ISampleModule1 instance = injector.getInstance(ISampleModule1.class);
		Assert.assertNotNull(instance);
		
		instance.testMethod1();
	}
}

class TestModule extends AbstractModule {

	@Override
	protected void configure() {
		// bind(ISampleModule1.class).to(SampleModuleImpl.class);
	}
}