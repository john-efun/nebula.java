/**
 * 
 */
package com.quanta.module.ioc.guice;

import com.google.inject.AbstractModule;

/**
 * 用來設定 Nebula Module 的 IoC 內部設定.
 * 
 * @author u99122010
 * 
 */
class NebulaIocModuleImpl extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {

		// Assign AppContext IoC ...
		// bind(IAppContext.class).to(AppContext.class).in(ThreadScope.Instance);
	}

}
