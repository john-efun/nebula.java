package com.quanta.module.common;

/**
 * {@link IBootStrapper} 的抽象類別.
 *  
 * @author ricky.chiang@quantatw.com
 * 
 */
public abstract class AbstractBootStrapper implements IBootStrapper {

	// Fields
	// ========================================
	/**
	 * 用來統計目前已經建立的 {@link IBootStrapper} 個數.
	 */
	public static int BootStrapperCount = 0;
	
	private int order = 0;
	
	// Methods
	// ========================================
	/**
	 * @return the order
	 */
	public int getOrder() {

		return order;
	}

	/**
	 * @param order
	 *            the order to set
	 */
	public void setOrder(int order) {

		this.order = order;
	}

}
