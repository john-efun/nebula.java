package com.quanta.module;

import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Scope;
import com.quanta.module.ioc.guice.GuiceModules;
import com.quanta.module.ioc.guice.ThreadScope;

/**
 * Singleton 物件，用來產生 {@link AppContext} instance.
 * 
 * @author u99122010
 * 
 */
public class AppContextFactory {

	// Static Fields
	// ========================================

	private static Log Logger = LogFactory.getLog(AppContextFactory.class);

	/**
	 * Singleton Instance.
	 */
	private final static AppContextFactory INSTANCE = new AppContextFactory();

	// private static IGuidProvider GuidProvider;
	// private static SolutionContext SolutionContext;

	// Fields
	// ========================================
	/**
	 * 設定 {@link AppContext} 在 Guice 中的 {@link Scope}.
	 */
	private Scope AppContextScope = ThreadScope.Instance;

	/**
	 * 裝載所有 Guice {@link Module} 的 {@link List}.
	 */
	private GuiceModules InjectModules = GuiceModules.getInstance();

	/**
	 * 存放 Guice 的 {@link Injector} 物件.
	 */
	private Injector RootInjector;

	// Static Getters, Setters
	// ========================================
	/**
	 * @return
	 */
	public static AppContextFactory getInstance() {

		return INSTANCE;
	}

	// Constructors
	// ========================================
	/**
	 * 建構子.
	 */
	private AppContextFactory() {

		Logger.debug("Init AppContextFactory .....");

		// 初始化所有 Module，用來註冊～
		init();

		// 利用指定的 GuiceModule 重新產生一個新的 Injector
		assignInjector();
	}

	// Methods
	// ========================================

	/**
	 * @return
	 */
	public IAppContext create() {

		return RootInjector.getInstance(IAppContext.class);
	}

	/**
	 * 初始化 GuiceModule 的設定.
	 * 
	 * @throws IOException
	 */
	public void init() {

		// GuiceModules guiceModules = GuiceModules.getInstance(true);

		// 載入設定檔案，確認 Kernel Location 的設定
		InjectModules.add(new AbstractModule() {

			@Override
			protected void configure() {

				if (Logger.isDebugEnabled()) {
					Logger.debug("Register NebulaConfigurator .....");
				}

				bind(NebulaConfigurator.class).toInstance(NebulaConfigurator.INSTANCE);
			}
		});

		// 註冊 AppContext 的 Guice Module
		InjectModules.add(new AbstractModule() {
			@Override
			protected void configure() {

				if (Logger.isDebugEnabled()) {
					Logger.debug("Register AppContext .....");
				}

				bind(IAppContext.class).to(AppContext.class).in(getAppContextScope());
			}
		});
	}

	public void destory() {

		// Close AppContext Factory....
	}

	/**
	 * 設定 {@link AppContext} 的生命週期.
	 * 
	 * @param scope
	 */
	public void setAppContextScope(Scope scope) {

		AppContextScope = scope;

		// 如果變更了 AppContext Scope, 就要重新 init 一次 Injector
		assignInjector();
	}

	/**
	 * 取得目前環境設定的 {@link AppContext} {@link Scope}
	 * 
	 * 預設 {@link AppContext} 的生命週期是 {@link ThreadScope}。
	 * 
	 * @return
	 */
	public Scope getAppContextScope() {

		return AppContextScope;
	}

	/**
	 * 利用指定的 {@link GuiceModules} 重新產生一個新的 {@link Injector}
	 */
	void assignInjector() {

		RootInjector = Guice.createInjector(InjectModules.getGuiceModules());
	}

	// Inner Classes
	// ========================================
}
