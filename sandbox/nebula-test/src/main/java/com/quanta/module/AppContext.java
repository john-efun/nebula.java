/**
 * 
 */
package com.quanta.module;

import java.util.UUID;

import com.google.inject.Inject;
import com.google.inject.Injector;

/**
 * Nebula.Java 核心物件.
 * 
 * @author u99122010
 * 
 */
public class AppContext implements IAppContext {

	// Fields
	// ========================================
	private Injector innerInjector;

	private String contextSignature = UUID.randomUUID().toString();

	/**
	 * 建構子.
	 */
	@Inject
	AppContext(Injector injector) {

		innerInjector = injector;
	}

	// Static Methods
	// ========================================

	
	/**
	 * Create a new {@link AppContext} instance.
	 * 
	 * @return
	 */
	public static IAppContext getInstance() {

		return AppContextFactory.getInstance().create();
	}

	// Methods
	// ========================================

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.quanta.module.IAppContext#getModule(java.lang.Class)
	 */
	public <T extends IModuleContext> T getModule(Class<T> clazz) {

		return innerInjector.getInstance(clazz);
	}

	@Override
	public String toString() {

		return String.format("AppContext[%s]", contextSignature);
	}
}
