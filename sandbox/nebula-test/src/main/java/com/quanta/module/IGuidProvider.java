package com.quanta.module;

/**
 * @author 99122010
 * 
 */
public interface IGuidProvider {

	public String provide();
}
