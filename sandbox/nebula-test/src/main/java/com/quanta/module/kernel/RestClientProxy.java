/**
 * 
 */
package com.quanta.module.kernel;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Client Proxy, Connect to Kernel Service
 * 
 * @author 99122010
 * 
 */
public class RestClientProxy {

	private static RestClientProxy Proxy;

	private String nebulaLocationRoot;
	private Client client;

	/**
	 * 用來存放已經產生的 {@link Client} 物件.
	 * 
	 * 因為產生 Client 的動作比較耗資源，所以先行產生好.
	 */
	private Map<String, WebResource> resources = Collections.synchronizedMap(new HashMap<String, WebResource>());

	/**
	 * @param root
	 */
	public static void Init(String root) {

		Proxy = new RestClientProxy(root);
	}

	/**
	 * @param root
	 * @return
	 */
	public static RestClientProxy getInstance() {

		return Proxy;
	}

	/**
	 * 建構子.
	 * 
	 * @param root
	 */
	RestClientProxy(String root) {

		nebulaLocationRoot = root;
	}

	/**
	 * @return the nebulaLocationRoot
	 */
	String getNebulaLocationRoot() {

		return nebulaLocationRoot;
	}

	// Methods
	// ========================================
	public Client getRestClient() {

		return client;
	}

	public ClientResponse get(String url, MediaType mediaType) {

		WebResource resource = getWebResource(url);

		return resource.type(mediaType).get(ClientResponse.class);
	}

	public ClientResponse post(String url, MediaType mediaType, Object req) {

		WebResource resource = getWebResource(url);

		ClientResponse resp = null;
		if (req == null) {
			resp = resource.type(mediaType).post(ClientResponse.class);
		}
		else {
			resp = resource.type(mediaType).post(ClientResponse.class, req);
		}

		return resp;
	}

	/**
	 * @param url
	 * @return
	 */
	private WebResource getWebResource(String url) {

		WebResource resource = null;
		if (resources.containsKey(url)) {
			resource = resources.get(url);
		}
		else {
			resource = getRestClient().resource(url);
		}
		return resource;
	}
}
