/**
 * 
 */
package com.quanta.module;

/**
 * {@link IModuleContext} 的抽象類別實作.
 * 
 * @author u99122010
 * 
 */
public abstract class AbstractModuleContext implements IModuleContext {

	// Fields
	// ========================================

	private IAppContext appContext;

	/**
	 * 建構子.
	 * 
	 * @param appContext
	 */
	protected AbstractModuleContext(IAppContext appContext) {

		this.appContext = appContext;
	}

	// Methods
	// ========================================

	/**
	 * @return the appContext
	 */
	public IAppContext getAppContext() {

		return appContext;
	}

}
