/**
 * 
 */
package com.quanta.module.ioc.guice;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.Scope;

/**
 * 實作 {@link Thread} 的 {@link Scope}.
 * 
 * @author 99122010
 * 
 */
public class ThreadScope implements Scope {

	public static final Scope Instance = new ThreadScope();

	private ThreadScope() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.inject.Scope#scope(com.google.inject.Key,
	 * com.google.inject.Provider)
	 */
	public <T> Provider<T> scope(final Key<T> key, final Provider<T> creator) {

		return new Provider<T>() {
			public T get() {

				ThreadLocalCache cache = ThreadLocalCache.getInstance();
				T value = cache.get(key);
				if (value == null) {
					value = creator.get();
					cache.add(key, value);
				}
				return value;
			}
		};
	}

	/**
	 * @author 99122010
	 * 
	 */
	private static final class ThreadLocalCache {
		private Map<Key<?>, Object> map = new HashMap<Key<?>, Object>();

		// use lazy init to avoid memory overhead when not using the scope?
		private static final ThreadLocal<ThreadLocalCache> THREAD_LOCAL = new ThreadLocal<ThreadLocalCache>() {
			@Override
			protected ThreadLocalCache initialValue() {

				return new ThreadLocalCache();
			}
		};

		// suppress warnings because the add method
		// captures the type
		@SuppressWarnings("unchecked")
		public <T> T get(Key<T> key) {

			return (T) map.get(key);
		}

		public <T> void add(Key<T> key, T value) {

			map.put(key, value);
		}

		public static ThreadLocalCache getInstance() {

			return THREAD_LOCAL.get();
		}
	}
}
