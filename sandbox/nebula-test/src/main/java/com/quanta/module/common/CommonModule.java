/**
 * 
 */
package com.quanta.module.common;

import com.quanta.module.AbstractModuleContext;
import com.quanta.module.IAppContext;

/**
 * @author 99122010
 * 
 */
public class CommonModule extends AbstractModuleContext {

	/**
	 * @param appContext
	 */
	protected CommonModule(IAppContext appContext) {

		super(appContext);
	}

}
