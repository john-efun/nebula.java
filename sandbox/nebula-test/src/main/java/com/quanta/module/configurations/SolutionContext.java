/**
 * 
 */
package com.quanta.module.configurations;

import java.io.Serializable;

/**
 * @author 99122010
 * 
 */
public class SolutionContext implements Serializable {

	private static final long serialVersionUID = 1L;

	// Fields
	// ========================================
	// private String kernelLocation;

	private SolutionInfo solutionInfo;
	private DeveloperContext developerInfo;

	/**
	 * @param solutionInfo
	 * @param developerInfo
	 */
	public SolutionContext(SolutionInfo solutionInfo, DeveloperContext developerInfo) {

		this.solutionInfo = solutionInfo;
		this.developerInfo = developerInfo;
	}

	// Methods
	// ========================================

	/**
	 * @return the solutionInfo
	 */
	public SolutionInfo getSolutionInfo() {

		return solutionInfo;
	}

	/**
	 * @return the developerInfo
	 */
	public DeveloperContext getDeveloperInfo() {

		return developerInfo;
	}

}
