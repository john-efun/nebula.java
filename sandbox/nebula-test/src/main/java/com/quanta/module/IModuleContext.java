package com.quanta.module;

/**
 * Nebula ModuleContext 所要實作的介面.
 * 
 * @author 99122010
 *
 */
public interface IModuleContext {

	/**
	 * 取得 {@link IModuleContext} 上層的 {@link IAppContext}. 
	 * 
	 * @return
	 */
	public IAppContext getAppContext();
}
