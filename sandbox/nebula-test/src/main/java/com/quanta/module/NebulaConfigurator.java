/**
 * 
 */
package com.quanta.module;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author 99122010
 * 
 */
public class NebulaConfigurator {

	/**
	 * {@link NebulaConfigurator} 的 Singleton Instance.
	 */
	public final static NebulaConfigurator INSTANCE = new NebulaConfigurator();
	private Log Logger = LogFactory.getLog(NebulaConfigurator.class);
	/**
	 * Nebula Properties File Location
	 */
	private static final String NEBULA_PROP_FILE = "/META-INF/nebula.properties";

	// Properties Keys Definitions
	// ========================================

	// Kernel Service Root Location
	private final static String NEBULA_KERNEL_LOC = "cloudplatform.paas.location";

	// Solution Info
	private final static String SOLUTION_PRODUCT_ID = "cloudplatform.solution.productId";
	private final static String SOLUTION_APPLICATION_ID = "cloudplatform.solution.applicationId";

	// Developer
	private final static String DEVELOPER_ACCOUNT = "cloudplatform.developer.account";
	private final static String DEVELOPPER_AUTHORITY_CODE = "cloudplatform.developer.authorityCode";

	// Fields
	// ========================================

	private String kernelServiceLocation;
	private String productId;
	private String applicationId;
	private String developAccount;
	private String authorityCode;

	/**
	 * 建構子.
	 */
	private NebulaConfigurator() {

		try {
			Properties props = new Properties();

			// Load Nebula Properties
			InputStream is = NebulaConfigurator.class.getResourceAsStream(NEBULA_PROP_FILE);
			Logger.debug("Load Nebula Properties ......" + (is != null));
			props.load(is);
			is.close();

			kernelServiceLocation = props.getProperty(NEBULA_KERNEL_LOC);
			productId = props.getProperty(SOLUTION_PRODUCT_ID);
			applicationId = props.getProperty(SOLUTION_APPLICATION_ID);
			developAccount = props.getProperty(DEVELOPER_ACCOUNT);
			authorityCode = props.getProperty(DEVELOPPER_AUTHORITY_CODE);

			if (Logger.isDebugEnabled()) {
				Logger.debug(StringUtils.repeat("=", 50));
				Logger.debug("Kernel Service  : " + getKernelServiceLocation());
				Logger.debug("Product Id      : " + getProductId());
				Logger.debug("Application Id  : " + getApplicationId());
				Logger.debug("Develop Account : " + getDevelopAccount());
				Logger.debug("Authority Code  : " + getAuthorityCode());
				Logger.debug(StringUtils.repeat("=", 50));
			}

		}
		catch (IOException ioe) {
			Logger.error("Load Nebula Properties Error: ", ioe);
			throw new RuntimeException(ioe);
		}
	}

	/**
	 * @return the kernelServiceLocation
	 */
	public String getKernelServiceLocation() {

		return kernelServiceLocation;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {

		return productId;
	}

	/**
	 * @return the applicationId
	 */
	public String getApplicationId() {

		return applicationId;
	}

	/**
	 * @return the developAccount
	 */
	public String getDevelopAccount() {

		return developAccount;
	}

	/**
	 * @return the authorityCode
	 */
	public String getAuthorityCode() {

		return authorityCode;
	}

}
