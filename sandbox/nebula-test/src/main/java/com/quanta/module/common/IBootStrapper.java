package com.quanta.module.common;

/**
 * Nebula Module 環境初始化.
 * 
 * @author ricky.chiang@quantatw.com
 * 
 */
public interface IBootStrapper {

	/**
	 * 用來統計目前已經建立的 {@link IBootStrapper} 個數.
	 */
	public static int BootStrapperCount = 0;

	/**
	 * BootStrapper 執行方法.
	 */
	public void run();
}
