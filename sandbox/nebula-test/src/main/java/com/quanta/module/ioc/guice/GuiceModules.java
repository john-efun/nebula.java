package com.quanta.module.ioc.guice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.inject.Module;

/**
 * 用來收集要用在 Guice 註冊用的 {@link Module} 物件.
 * 
 * @author 99122010
 * 
 */
public class GuiceModules {

	private final static GuiceModules INSTANCE = new GuiceModules();

	private List<Module> modules = Collections.synchronizedList(new ArrayList<Module>());

	/**
	 * @return
	 */
	public static Iterable<Module> getModules() {

		return INSTANCE.getGuiceModules();
	}

	/**
	 * @return
	 */
	public static GuiceModules getInstance() {

		return getInstance(false);
	}

	/**
	 * @param isResetModules
	 * @return
	 */
	public static GuiceModules getInstance(boolean isResetModules) {

		if (isResetModules) {
			INSTANCE.reinit();
		}

		return INSTANCE;
	}

	/**
	 * @param module
	 */
	public void add(Module module) {

		modules.add(module);
	}

	/**
	 * @return
	 */
	public Iterable<Module> getGuiceModules() {

		return modules;
	}

	/**
	 * 
	 */
	private void reinit() {

		// modules = Collections.synchronizedList(new ArrayList<Module>());
		modules.clear();
	}

}
