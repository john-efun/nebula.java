package com.quanta.module;

/**
 * {@link AppContext} 介面.
 * 
 * @author 99122010
 * 
 */
public interface IAppContext {

	/**
	 * @param clazz
	 * @return
	 */
	public <T extends IModuleContext> T getModule(Class<T> clazz);
}