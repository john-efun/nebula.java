package com.quanta.module.configurations;

import java.io.Serializable;

/**
 * @author 99122010
 * 
 */
public class SolutionInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String productId;
	private String solutionId;
	private String applicationId;

	/**
	 * @param productId
	 * @param solutionId
	 * @param applicationId
	 */
	public SolutionInfo(String productId, String solutionId, String applicationId) {

		this.productId = productId;
		this.solutionId = solutionId;
		this.applicationId = applicationId;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {

		return productId;
	}

	/**
	 * @return the solutionId
	 */
	public String getSolutionId() {

		return solutionId;
	}

	/**
	 * @return the applicationId
	 */
	public String getApplicationId() {

		return applicationId;
	}

}
