package com.quanta.module;

import com.quanta.module.configurations.SolutionContext;

public interface ISolutionContextProvider {

	public SolutionContext Provide();
}
