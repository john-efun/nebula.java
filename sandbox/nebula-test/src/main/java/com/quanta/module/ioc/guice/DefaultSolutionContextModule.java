/**
 * 
 */
package com.quanta.module.ioc.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.quanta.module.NebulaConfigurator;
import com.quanta.module.configurations.DeveloperContext;
import com.quanta.module.configurations.SolutionContext;
import com.quanta.module.configurations.SolutionInfo;

/**
 * 
 * @author ricky.chiang@quantatw.com
 * 
 */
public class DefaultSolutionContextModule extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {

		// 預設的 Guid 是 ProductId
		bind(String.class).annotatedWith(Names.named("GUID")).toInstance(NebulaConfigurator.INSTANCE.getProductId());

		// 預設 SolutionContext 提供的 Scope 是 Singleton~
		bind(SolutionContext.class).toProvider(SolutionContextProvider.class).asEagerSingleton();
	}

	/**
	 *
	 * @author ricky.chiang@quantatw.com
	 *
	 */
	static class SolutionContextProvider implements Provider<SolutionContext> {

		@Inject
		@Named("GUID")
		Provider<String> guidProvider;

		@Inject
		NebulaConfigurator configurator;

		public com.quanta.module.configurations.SolutionContext get() {

			SolutionInfo solutionInfo = new SolutionInfo(configurator.getProductId(), guidProvider.get(),
					configurator.getApplicationId());

			DeveloperContext developerInfo = new DeveloperContext(configurator.getDevelopAccount(),
					configurator.getAuthorityCode());

			return new SolutionContext(solutionInfo, developerInfo);
		}

	}
}
