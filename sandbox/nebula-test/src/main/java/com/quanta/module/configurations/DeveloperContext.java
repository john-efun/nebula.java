package com.quanta.module.configurations;

import java.io.Serializable;

/**
 * @author 99122010
 * 
 */
public class DeveloperContext implements Serializable {

	private static final long serialVersionUID = 1L;

	private String account;
	private String authorityCode;

	/**
	 * @param account
	 * @param authorityCode
	 */
	public DeveloperContext(String account, String authorityCode) {

		this.account = account;
		this.authorityCode = authorityCode;
	}

	/**
	 * @return the account
	 */
	public String getAccount() {

		return account;
	}

	/**
	 * @return the authorityCode
	 */
	public String getAuthorityCode() {

		return authorityCode;
	}

}
