package com.quanta.module.bootstrapper;

import com.quanta.module.AppContext;


/**
 * @author ricky.chiang@quantatw.com
 * 
 */
public interface IBootStrapper {

	/**
	 * Run BootStrapper Code.
	 */
	public void run(AppContext appContext);
}
