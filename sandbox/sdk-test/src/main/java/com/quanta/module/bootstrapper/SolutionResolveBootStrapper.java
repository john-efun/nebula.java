// ~ Package Declaration
// ==================================================
/**
 * 
 */
package com.quanta.module.bootstrapper;

import java.util.concurrent.Future;

import com.quanta.module.AppContext;
import com.quanta.module.ICommonModule;
import com.quanta.module.SolutionContext;

// ~ Comments
// ==================================================
/**
 * @author ricky.chiang@quantatw.com
 * 
 */
public class SolutionResolveBootStrapper extends AbstractBootStrapper implements IBootStrapper {

	// ~ Static Members
	// ==================================================

	// ~ Fields
	// ==================================================

	// ~ Constructors
	// ==================================================

	// ~ Methods
	// ==================================================
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.quanta.module.bootstrapper.IBootStrapper#run(com.quanta.module.AppContext
	 * )
	 */
	public void run(AppContext appContext) {

		appContext.setSolutionContext(new SolutionContext(findSolutionId(appContext)));

	}

	/**
	 * delegate {@link ICommonModule#resolveSolutionId(String)} to find solutionId.
	 * 
	 * @param appContext
	 * @return
	 */
	private Future<String> findSolutionId(AppContext appContext) {

		return appContext.getModule(ICommonModule.class).resolveSolutionId(appContext.getGuid());
	}

}
