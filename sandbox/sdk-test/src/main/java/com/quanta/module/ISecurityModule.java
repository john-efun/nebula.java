// ~ Package Declaration
// ==================================================
package com.quanta.module;

// ~ Comments
// ==================================================
public interface ISecurityModule {
	
	/**
	 * @return
	 */
	public String getCipPasswordEncode();
}
