package com.quanta.module.internal;

import com.sun.jersey.api.client.WebResource;

/**
 * @author ricky.chiang@quantatw.com
 * 
 */
public interface IKernelServiceProxy {

	/**
	 * @param relativeUrl
	 * @return
	 */
	public abstract WebResource createResouce(String relativeUrl);

	/**
	 * @param relativeUrl
	 * @param urlParams
	 * @return
	 */
	public abstract WebResource createResouce(String relativeUrl, Object... urlParams);

}