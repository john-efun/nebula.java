// ~ Package Declaration
// ==================================================
/**
 * 
 */
package com.quanta.module;

// ~ Comments
// ==================================================
/**
 * @author ricky.chiang@quantatw.com
 * 
 */
public interface ILogModule {
	
	/**
	 * @param ex
	 */
	public void logError(Exception ex);
}
