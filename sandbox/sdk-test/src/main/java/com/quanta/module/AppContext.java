/**
 * 
 */
package com.quanta.module;

import java.io.Serializable;
import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.UUID;

import com.quanta.module.bootstrapper.IBootStrapper;
import com.quanta.module.configuration.NebulaConfigurator;

/**
 * Nebula Module Core Class.
 * 
 * @author ricky.chiang@quantatw.com
 * 
 */
public class AppContext implements IAppContext, Serializable {

	// ~ Static Members
	// ==================================================
	private static final long serialVersionUID = 1L;

	private static boolean isAuthorized = false;
	/**
	 * @param isAuthorized
	 */
	public static void setAuthorized(boolean isAuthorized) {

		AppContext.isAuthorized = isAuthorized;
	}

	/**
	 * @return
	 */
	public static boolean isAuthorized() {

		return isAuthorized;
	}
	
	// ~ Fields
	// ==================================================
	private String contextSignature = UUID.randomUUID().toString();

	private String guid = NebulaConfigurator.getInstance().getProductId();

	private SolutionContext solutionContext;


	// ~ Constructors
	// ==================================================

	public AppContext() {

		init();
	}

	public AppContext(String guid) {

		this.guid = guid;

		init();
	}

	// ~ Methods
	// ==================================================

	/**
	 * Initial {@link AppContext}.
	 */
	private void init() {

		// Run BootStrapper
		ServiceLoader<IBootStrapper> bootStrapperLoaders = ServiceLoader.load(IBootStrapper.class);
		for (IBootStrapper b : bootStrapperLoaders) {
			b.run(this);
		}

		// Developer Authorize

		// Init SolutionContext

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.quanta.module.IAppContext#getSoolutionContext()
	 */
	public SolutionContext getSolutionContext() {

		return solutionContext;
	}

	/**
	 * @param solutionContext
	 */
	public void setSolutionContext(SolutionContext solutionContext) {

		this.solutionContext = solutionContext;
	}

	/**
	 * @return
	 */
	public String getGuid() {

		return guid;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.quanta.module.IAppContext#getModule(java.lang.Class)
	 */
	public <T> T getModule(Class<T> clazz) {

		
		
		ServiceLoader<T> serviceLoader = ServiceLoader.load(clazz);

		Iterator<T> iterator = serviceLoader.iterator();
		if (!iterator.hasNext()) {

			throw new IllegalArgumentException(String.format("Module[%s] is no implementation ! ", clazz.getName()));
		}
		T module = iterator.next();

		// Assign AppContext to ModuleContext
		try {
			AbstractModuleContext m = AbstractModuleContext.class.cast(module);

			m.setAppContext(this);
		}
		catch (ClassCastException cce) {
			throw new IllegalArgumentException(String.format("Module Implementation [%s] need extends %s! ",
					module.getClass().getName(), AbstractModuleContext.class.getName()));
		}

		return module;
	}

	@Override
	protected void finalize() throws Throwable {

		super.finalize();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return super.toString() + String.format("[%s]", contextSignature);
	}
}
