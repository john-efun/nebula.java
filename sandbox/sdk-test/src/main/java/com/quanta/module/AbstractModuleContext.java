// ~ Package Declaration
// ==================================================
package com.quanta.module;

// ~ Comments
// ==================================================

/**
 * @author ricky.chiang@quantatw.com
 * 
 */
public abstract class AbstractModuleContext {

	// ~ Static Members
	// ==================================================

	// ~ Fields
	// ==================================================
	private IAppContext appContext;

	// ~ Constructors
	// ==================================================

	// ~ Methods
	// ==================================================

	/**
	 * @return the appContext
	 */
	public IAppContext getAppContext() {

		return appContext;
	}

	/**
	 * @param appContext
	 */
	void setAppContext(IAppContext appContext) {

		this.appContext = appContext;
	}
}
