package com.quanta.module;

import java.io.Serializable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quanta.module.configuration.NebulaConfigurator;

/**
 * @author ricky.chiang@quantatw.com
 * 
 */
public class SolutionContext implements Serializable {

	// ~ Static Members
	// ==================================================
	private static final long serialVersionUID = 1L;

	private static Logger Logger = LoggerFactory.getLogger(SolutionContext.class);
	// ~ Fields
	// ==================================================
	private Future<String> solutionId;

	private NebulaConfigurator config = NebulaConfigurator.getInstance();

	// ~ Constructors
	// ==================================================
	public SolutionContext(Future<String> solutionId) {

		this.solutionId = solutionId;
	}

	// ~ Methods
	// ==================================================

	/**
	 * @return
	 * @see com.quanta.module.configuration.NebulaConfigurator#getProductId()
	 */
	public String getProductId() {

		return config.getProductId();
	}

	/**
	 * @return
	 * @see com.quanta.module.configuration.NebulaConfigurator#getApplicationId()
	 */
	public String getApplicationId() {

		return config.getApplicationId();
	}

	/**
	 * @return
	 * @see com.quanta.module.configuration.NebulaConfigurator#getDevelopAccount()
	 */
	public String getDevelopAccount() {

		return config.getDevelopAccount();
	}

	/**
	 * @return
	 * @see com.quanta.module.configuration.NebulaConfigurator#getAuthorityCode()
	 */
	public String getAuthorityCode() {

		return config.getAuthorityCode();
	}

	/**
	 * @return
	 */
	public String getSolutionId() {

		try {
			return solutionId.get();
		}
		catch (InterruptedException e) {
			Logger.error(e.getMessage() + "\n " + e.getStackTrace());
			
			// throw new RuntimeException(e.get);
			return "";
		}
		catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
