/**
 * 
 */
package com.quanta.module.bootstrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ricky.chiang@quantatw.com
 * 
 */
public abstract class AbstractBootStrapper implements IBootStrapper {

	// Fields
	// ========================================

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
}
