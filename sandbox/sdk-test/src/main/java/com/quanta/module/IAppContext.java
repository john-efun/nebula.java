package com.quanta.module;

/**
 * @author ricky.chiang@quantatw.com
 * 
 */
public interface IAppContext {

	/**
	 * @return
	 */
	public SolutionContext getSolutionContext();

	/**
	 * @return
	 */
	public <T> T getModule(Class<T> clazz);

//	/**
//	 * 
//	 * 
//	 * @return
//	 */
//	public boolean isAuthorized();

}
