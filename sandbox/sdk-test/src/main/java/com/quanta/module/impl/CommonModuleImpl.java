// ~ Package Declaration
// ==================================================
/**
 * 
 */
package com.quanta.module.impl;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import com.quanta.module.AbstractModuleContext;
import com.quanta.module.ICommonModule;
import com.quanta.module.internal.KernelServiceProxy;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

// ~ Comments
// ==================================================
/**
 * @author ricky.chiang@quantatw.com
 * 
 */
public class CommonModuleImpl extends AbstractModuleContext implements ICommonModule {

	// ~ Static Members
	// ==================================================

	// ~ Fields
	// ==================================================

	// ~ Constructors
	// ==================================================
	
	// ~ Methods
	// ==================================================
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.quanta.module.ICommonModule#authenticateDeveloper(java.lang.String,
	 * java.lang.String)
	 */
	public boolean authenticateDeveloper(String account, String authorityCode) {

		String input = String.format("{ \"Account\" : \"%s\", \"AuthorityCode\": \"%s\" }", account, authorityCode);

		WebResource res = new KernelServiceProxy().createResouce("Common/access");

		ClientResponse resp = res.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, input);

		return (resp.getStatus() == Status.OK.getStatusCode());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.quanta.module.ICommonModule#resolveSolutionId(java.lang.String)
	 */
	public Future<String> resolveSolutionId(String guid) {

		return Executors.newSingleThreadExecutor().submit(new ResolveSolutionProvider(guid));
	}

	/**
	 * @author ricky.chiang@quantatw.com
	 * 
	 */
	private class ResolveSolutionProvider implements Callable<String> {

		private String guid;

		/**
		 * @param guid
		 */
		public ResolveSolutionProvider(String guid) {

			this.guid = guid;
		}

		public String call() throws Exception {

			WebResource res = new KernelServiceProxy().createResouce("Common/solution/%s", guid);

			ClientResponse resp = res.get(ClientResponse.class);

			if (Status.OK.getStatusCode() == resp.getStatus()) {
				String solutionId = resp.getEntity(String.class);
				System.out.println("Solution ID : " + solutionId);
				return solutionId;
			}
			else {
				throw new RuntimeException("Can't resolve solutionid");
			}
		}
	}
}
