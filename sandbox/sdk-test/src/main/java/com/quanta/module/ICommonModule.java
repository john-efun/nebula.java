// ~ Package Declaration
// ==================================================
package com.quanta.module;

import java.util.concurrent.Future;

// ~ Comments
// ==================================================
/**
 * 共用的模組, 主要是驗證機制與 Solution Id 的取得.
 * 
 * @author ricky.chiang@quantatw.com
 * 
 */
public interface ICommonModule {

	/**
	 * 驗證使用者.
	 * 
	 * @param account
	 * @param authorityCode
	 * @return
	 */
	public boolean authenticateDeveloper(String account, String authorityCode);

	/**
	 * 取得 Solution Id.
	 * 
	 * @param guid
	 * @return
	 */
	public Future<String> resolveSolutionId(String guid);
}
