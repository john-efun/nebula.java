/**
 * 
 */
package com.quanta.module.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ricky.chiang@quantatw.com
 * 
 */
public class NebulaConfigurator {

	private Logger logger = LoggerFactory.getLogger(NebulaConfigurator.class);

	// @InjectLogger Logger logger;

	/**
	 * {@link NebulaConfigurator} 的 Singleton Instance.
	 */
	private static NebulaConfigurator INSTANCE;
	/**
	 * Nebula Properties File Location
	 */
	private static final String NEBULA_PROP_FILE = "/META-INF/nebula.properties";

	// Properties Keys Definitions
	// ========================================

	// Kernel Service Root Location
	private final static String NEBULA_KERNEL_LOC = "cloudplatform.paas.location";

	// Solution Info
	private final static String SOLUTION_PRODUCT_ID = "cloudplatform.solution.productId";
	private final static String SOLUTION_APPLICATION_ID = "cloudplatform.solution.applicationId";

	// Developer
	private final static String DEVELOPER_ACCOUNT = "cloudplatform.developer.account";
	private final static String DEVELOPPER_AUTHORITY_CODE = "cloudplatform.developer.authorityCode";

	public static NebulaConfigurator getInstance() {

		if (INSTANCE == null) {
			INSTANCE = new NebulaConfigurator();
		}

		return INSTANCE;
	}

	// Fields
	// ========================================

	private String kernelServiceLocation;
	private String productId;
	private String applicationId;
	private String developAccount;
	private String authorityCode;

	/**
	 * 建構子.
	 * 
	 * @param kernelServiceLocation
	 * @param productId
	 * @param applicationId
	 * @param developAccount
	 * @param authorityCode
	 */
	public NebulaConfigurator(String kernelServiceLocation, String productId, String applicationId,
			String developAccount, String authorityCode) {

		this.kernelServiceLocation = kernelServiceLocation;
		this.productId = productId;
		this.applicationId = applicationId;
		this.developAccount = developAccount;
		this.authorityCode = authorityCode;
	}

	/**
	 * 建構子.
	 */
	NebulaConfigurator() {

		try {
			Properties props = new Properties();

			// Load Nebula Properties
			InputStream is = NebulaConfigurator.class.getResourceAsStream(NEBULA_PROP_FILE);
			logger.debug("Load Nebula Properties ......" + (is != null));
			props.load(is);
			is.close();

			kernelServiceLocation = props.getProperty(NEBULA_KERNEL_LOC);
			productId = props.getProperty(SOLUTION_PRODUCT_ID);
			applicationId = props.getProperty(SOLUTION_APPLICATION_ID);
			developAccount = props.getProperty(DEVELOPER_ACCOUNT);
			authorityCode = props.getProperty(DEVELOPPER_AUTHORITY_CODE);

			if (logger.isDebugEnabled()) {
				logger.debug(StringUtils.repeat("=", 50));
				logger.debug("Kernel Service  : " + getKernelServiceLocation());
				logger.debug("Product Id      : " + getProductId());
				logger.debug("Application Id  : " + getApplicationId());
				logger.debug("Develop Account : " + getDevelopAccount());
				logger.debug("Authority Code  : " + getAuthorityCode());
				logger.debug(StringUtils.repeat("=", 50));
			}

		}
		catch (IOException ioe) {
			logger.error("Load Nebula Properties Error: ", ioe);
			throw new RuntimeException(ioe);
		}
	}

	/**
	 * @return the kernelServiceLocation
	 */
	public String getKernelServiceLocation() {

		return kernelServiceLocation;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {

		return productId;
	}

	/**
	 * @return the applicationId
	 */
	public String getApplicationId() {

		return applicationId;
	}

	/**
	 * @return the developAccount
	 */
	public String getDevelopAccount() {

		return developAccount;
	}

	/**
	 * @return the authorityCode
	 */
	public String getAuthorityCode() {

		return authorityCode;
	}

}
