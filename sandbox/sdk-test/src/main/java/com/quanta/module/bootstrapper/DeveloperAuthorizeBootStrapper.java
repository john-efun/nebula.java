/**
 * 
 */
package com.quanta.module.bootstrapper;

import com.quanta.module.AppContext;
import com.quanta.module.ICommonModule;
import com.quanta.module.configuration.NebulaConfigurator;

/**
 * @author ricky.chiang@quantatw.com
 * 
 */
public class DeveloperAuthorizeBootStrapper extends AbstractBootStrapper {

	private NebulaConfigurator config = NebulaConfigurator.getInstance();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.quanta.module.bootstrapper.IBootStrapper#run()
	 */
	public void run(AppContext appContext) {

		if (!AppContext.isAuthorized()) {
			logger.debug("Developer Authorizing..... ");

			String developAccount = config.getDevelopAccount();
			String authorityCode = config.getAuthorityCode();

			ICommonModule commonModule = appContext.getModule(ICommonModule.class);

			AppContext.setAuthorized(commonModule.authenticateDeveloper(developAccount, authorityCode));
		}
	}
	//
	// /**
	// * 驗證 Developer User Account 的正確性.
	// *
	// * @param developAccount
	// * @param authorityCode
	// */
	// private boolean authorizeDeveloper(String developAccount, String
	// authorityCode) {
	//
	// String input =
	// String.format("{ \"account\" : \"%s\", \"authorityCode\": \"%s\" }",
	// developAccount,
	// authorityCode);
	//
	// WebResource res = new KernelServiceProxy().createResouce("access");
	//
	// ClientResponse resp =
	// res.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, input);
	//
	// return (resp.getStatus() == Status.OK.getStatusCode());
	// // if (resp.getStatus() != Status.OK.getStatusCode()) {
	// // throw new RuntimeException("Developer Authorize Failed!!!");
	// // }
	// // else {
	// // logger.info("Developer Authorized!!! ");
	// //
	// // }
	//
	// }

}
