/**
 * 
 */
package com.quanta.module.internal;

import com.quanta.module.configuration.NebulaConfigurator;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.json.JSONConfiguration;

/**
 * @author ricky.chiang@quantatw.com
 * 
 */
public class KernelServiceProxy implements IKernelServiceProxy {

	private static Client client;

	// PreCreate Kernel Proxy Client
	static {
		ClientConfig restConfig = new DefaultClientConfig();
		restConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING,
				Boolean.TRUE);
		client = Client.create(restConfig);

		client.addFilter(new ClientFilter() {
			@Override
			public ClientResponse handle(ClientRequest cr)
					throws ClientHandlerException {

				NebulaConfigurator config = NebulaConfigurator.getInstance();

				cr.getHeaders().putSingle("Nebula-ProductId",
						config.getProductId());
				cr.getHeaders().putSingle("Nebula-ApplicationId",
						config.getApplicationId());

				return getNext().handle(cr);
			}
		});

		// Add User Authentication Filter
		client.addFilter(new HTTPBasicAuthFilter("", ""));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.quanta.module.internal.IKernelServiceProxy#createResouce(java.lang
	 * .String)
	 */
	public WebResource createResouce(String relativeUrl) {

		return client.resource(String.format("http://%s/%s",
				getKernelLocation(), relativeUrl));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.quanta.module.internal.IKernelServiceProxy#createResouce(java.lang
	 * .String, java.lang.Object[])
	 */
	public WebResource createResouce(String relativeUrl, Object... urlParams) {

		return createResouce(String.format(relativeUrl, urlParams));
	}

	/**
	 * @return the kernelLocation
	 */
	public String getKernelLocation() {

		return NebulaConfigurator.getInstance().getKernelServiceLocation();
	}

}
