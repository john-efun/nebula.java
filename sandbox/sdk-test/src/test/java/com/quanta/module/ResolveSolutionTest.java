// ~ Package Declaration
// ==================================================
package com.quanta.module;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import junit.framework.Assert;

import org.junit.Test;

import com.quanta.module.configuration.NebulaConfigurator;
import com.quanta.module.internal.KernelServiceProxy;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.WebResource;

// ~ Comments
// ==================================================
public class ResolveSolutionTest {

	// ~ Static Members
	// ==================================================

	// ~ Fields
	// ==================================================

	// ~ Constructors
	// ==================================================

	// ~ Methods
	// ==================================================
	//@Test
	public void testResolveSolutionId() throws InterruptedException, ExecutionException {

		FutureTask<String> f = new FutureTask<String>(new ResolveSolutionProvider("Northwind"));
		Executor executor = Executors.newFixedThreadPool(100);
		
		System.out.println("Prepare Get Solution id");
		executor.execute(f);
		
		System.out.println("Assert Solution id");
		Assert.assertEquals("Northwind", f.get());
		
	}
}

class ResolveSolutionProvider implements Callable<String> {

	private String guid;

	public ResolveSolutionProvider() {

		this(NebulaConfigurator.getInstance().getProductId());
	}

	/**
	 * @param guid
	 */
	public ResolveSolutionProvider(String guid) {

		this.guid = guid;
	}

	public String call() throws Exception {

		WebResource res = new KernelServiceProxy().createResouce("Common/solution/%s", guid);
		
		ClientResponse resp = res.get(ClientResponse.class);

		if (Status.OK.getStatusCode() == resp.getStatus()) {
			String solutionId = resp.getEntity(String.class);
			System.out.println("Solution ID : " + solutionId);
			return solutionId;
		}
		else {
			throw new RuntimeException("Can't resolve solutionid");
		}
	}
}